/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE = ''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS = @@UNIQUE_CHECKS, UNIQUE_CHECKS = 0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS = @@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS = 0 */;
/*!40101 SET @OLD_SQL_MODE = @@SQL_MODE, SQL_MODE = 'NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES = @@SQL_NOTES, SQL_NOTES = 0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS */`shop` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `shop`;

DROP TABLE IF EXISTS `products`;

CREATE TABLE `products`
(
    id        BIGINT        NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name      VARCHAR(128)  NOT NULL,
    price     DOUBLE(10, 2) NOT NULL,
    image_url VARCHAR(256)  NOT NULL
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

INSERT INTO `products` (id, name, price, image_url)
VALUES (1, '雪碧', 4.00,
        'https://https://bkssl.bdimg.com/static/wiki-album/widget/picture/pictureDialog/resource/img/img-bg_86e1dfc.gif'),
       (2, '可口可乐', 3.00,
        'https://upload.wikimedia.org/wikipedia/commons/5/5d/Checker-16x16.png');

/*!40101 SET SQL_MODE = @OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS = @OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES = @OLD_SQL_NOTES */;