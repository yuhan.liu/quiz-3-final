package com.twuc.backend.controller;

import com.twuc.backend.domain.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("/api/orders")
public class OrderController {
    //TODO: controller 一般不直接引用Repository
    //TODO: field injection not recommended
    @Autowired
    private OrderRepository orderRepository;
    //TODO: field injection not recommended
    @Autowired
    private ItemRepository itemRepository;
    //TODO: field injection not recommended
    @Autowired
    private ProductRepository productRepository;

    //TODO: 不用括号
    @PostMapping("")
    @ResponseStatus(HttpStatus.CREATED)
    public void createOrder(@RequestBody PostOrderRequest postOrderRequest) {
        Map<Long, Integer> map = postOrderRequest.getMap();
        map.forEach((productId, num) -> {
            Products product = productRepository.getOne(productId);

            Order order = new Order();
            Item orderItem = new Item(num);

            orderItem.setProduct(product);
            order.addOrderItem(orderItem);

            orderRepository.save(order);
            itemRepository.save(orderItem);
        });
    }
}
