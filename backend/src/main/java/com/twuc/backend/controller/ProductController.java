package com.twuc.backend.controller;

import com.twuc.backend.domain.PostProductRequest;
import com.twuc.backend.domain.Products;
import com.twuc.backend.domain.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/products")
public class ProductController {
    //TODO: field injection not recommended
    //TODO: controller 一般不直接引用Repository
    @Autowired
    private ProductRepository productRepository;

    //TODO: 不用括号
    @PostMapping("")
    public ResponseEntity addProduct(@Valid @RequestBody PostProductRequest postProductRequest){
        //TODO: 可以在PostProductRequest里面写个toDomain的方法
        String name = postProductRequest.getName();
        Double price = postProductRequest.getPrice();
        String imageUrl = postProductRequest.getImageUrl();
        Products product = new Products(name,price,imageUrl);
        productRepository.saveAndFlush(product);
        return ResponseEntity.status(HttpStatus.OK)
                .header("Location", "/api/products/" + product.getId())
                .build();
    }
    @GetMapping("")
    public List<Products> getProductList(){
        return productRepository.findAll();
    }
}
