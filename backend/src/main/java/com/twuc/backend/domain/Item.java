package com.twuc.backend.domain;

import javax.persistence.*;

@Entity
public class Item {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false)
    private Integer count;
    @OneToOne
    private Products products;
    @ManyToOne
    private Order order;

    public Item() {
    }

    public Item(Integer count) {
        this.count = count;
    }

    public Long getId() {
        return id;
    }

    public Integer getCount() {
        return count;
    }

    public Products getProducts() {
        return products;
    }

    public Order getOrder() {
        return order;
    }

    public void setProduct(Products product) {
        this.products = product;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}
