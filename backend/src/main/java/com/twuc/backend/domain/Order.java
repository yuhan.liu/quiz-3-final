package com.twuc.backend.domain;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue
    private Long id;
    @OneToMany(mappedBy = "order", cascade = CascadeType.ALL)
    private List<Item> Items = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public List<Item> getItems() {
        return Items;
    }

    public void addOrderItem(Item Item) {
        Items.add(Item);
    }
}
