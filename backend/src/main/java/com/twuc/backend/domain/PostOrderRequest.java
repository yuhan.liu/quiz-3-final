package com.twuc.backend.domain;

import java.util.Map;

//TODO: 数据格式怪怪的
public class PostOrderRequest {
    private Map<Long, Integer> map;

    public PostOrderRequest(Map<Long, Integer> map) {
        this.map = map;
    }

    public PostOrderRequest() {
    }

    public Map<Long, Integer> getMap() {
        return map;
    }
}
