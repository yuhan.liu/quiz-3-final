package com.twuc.backend.domain;

import javax.validation.constraints.NotNull;

//TODO: bad naming
public class PostProductRequest {
    @NotNull
    private String name;
    @NotNull
    private Double price;
    @NotNull
    private String imageUrl;

    public PostProductRequest() {
    }

    public PostProductRequest(@NotNull String name, @NotNull Double price, @NotNull String imageUrl) {
        this.name = name;
        this.price = price;
        this.imageUrl = imageUrl;
    }

    public String getName() {
        return name;
    }

    public Double getPrice() {
        return price;
    }

    public String getImageUrl() {
        return imageUrl;
    }
}
