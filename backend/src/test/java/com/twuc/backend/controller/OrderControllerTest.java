package com.twuc.backend.controller;

import com.twuc.backend.ApiTestBase;
import com.twuc.backend.domain.Products;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

class OrderControllerTest extends ApiTestBase {
    @BeforeEach
    void setUp() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Products("鸡腿", 5.00, "jiTui.jpg"))));
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Products("鸭腿", 6.00, "yaTui.jpg"))));
    }
    @Test
    void should_add_product_to_product_list() {
        HashMap<Long, Integer> map = new HashMap<>();
    }
}