package com.twuc.backend.controller;

import com.twuc.backend.ApiTestBase;
import com.twuc.backend.domain.Products;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


class ProductControllerTest extends ApiTestBase {
    @Test
    void should_add_new_product() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Products("鸡腿", 5.00, "jiTui.jpg"))))
                .andExpect(status().is2xxSuccessful())
                .andExpect(header().string("Location", "/api/products/1"));
    }

    @Test
    void should_get_product_list() throws Exception {
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Products("鸡腿", 5.00, "jiTui.jpg"))));
        mockMvc.perform(post("/api/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(serialize(new Products("鸭腿", 6.00, "yaTui.jpg"))));
        mockMvc.perform(get("/api/products"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)));
    }
}