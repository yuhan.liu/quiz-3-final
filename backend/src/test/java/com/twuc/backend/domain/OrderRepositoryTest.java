package com.twuc.backend.domain;

import com.twuc.backend.ApiTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class OrderRepositoryTest extends ApiTestBase {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ItemRepository itemRepository;

    @Test
    void should_create_order() {
        Order order = new Order();
        Products products = new Products("鸡腿", 5.00, "jiTui.jpg");
        Item orderItem = new Item(1);

        productRepository.saveAndFlush(products);
        itemRepository.saveAndFlush(orderItem);
        orderRepository.saveAndFlush(order);

        order.addOrderItem(orderItem);
        orderItem.setProduct(products);

        Order savedOrder = orderRepository.getOne(order.getId());
        assertEquals(1, savedOrder.getItems().size());
    }

    @Test
    void should_delete_order() {
        Order order = new Order();
        Products products = new Products("鸡腿", 5.00, "jiTui.jpg");
        Item orderItem = new Item(1);

        productRepository.saveAndFlush(products);
        itemRepository.saveAndFlush(orderItem);
        orderRepository.saveAndFlush(order);

        order.addOrderItem(orderItem);
        orderItem.setProduct(products);
        Order savedOrder = orderRepository.getOne(order.getId());
        orderRepository.delete(savedOrder);

        Optional<Order> optionalOrder = orderRepository.findById(savedOrder.getId());
        assertFalse(optionalOrder.isPresent());
    }
}