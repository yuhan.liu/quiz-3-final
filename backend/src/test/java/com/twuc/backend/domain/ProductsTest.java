package com.twuc.backend.domain;

//TODO: unused import
import com.twuc.backend.domain.Products;
import com.twuc.backend.domain.ProductRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import static org.junit.jupiter.api.Assertions.assertNotNull;

@DataJpaTest(showSql = false)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class ProductsTest {
    @Autowired
    ProductRepository repository;

    @Test
    void should_add_product() {
        Products product = new Products("鸡腿", 5.00, "jiTui.jpg");
        repository.saveAndFlush(product);
        Products savedProduct = repository.getOne(product.getId());
        assertNotNull(savedProduct);
    }
}
