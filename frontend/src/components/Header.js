import './Header.less'
import React from 'react';
import {BrowserRouter as Router, NavLink} from 'react-router-dom';
import {Route} from 'react-router';
import Products from "../products/components/Products";
import Add from "./Add";
import ProductList from "./ProductList";
import {connect} from "react-redux";
import getProductList from "../products/actions/getProductList";

class Header extends React.Component {
    constructor(props, context) {
        super(props, context);
    }

    componentDidMount() {
        this.props.getProduct();
    }

    render() {
        return (
            <div className='header'>
                <Router>
                    <div>
                        <ul>
                            <li>
                                <NavLink exact to='/' activeStyle={
                                    {
                                        backgroundColor: 'blue'
                                    }
                                }>商城</NavLink>
                            </li>
                            <li>
                                <NavLink exact to='/order' activeStyle={
                                    {
                                        backgroundColor: 'blue'
                                    }
                                }>订单</NavLink>
                            </li>
                            <li><NavLink exact to='/add' activeStyle={
                                {
                                    backgroundColor: 'blue'
                                }
                            }>添加商品</NavLink>
                            </li>
                        </ul>
                        <switch>
                            <Route exact path='/' component={Products}/>
                            <Route exact path='/order' component={ProductList}/>
                            <Route exact path='/add' component={Add}/>
                        </switch>
                    </div>
                </Router>
            </div>
        )
    }
}

function mapDispatchToProps(dispatch) {
    return {
        getProduct: () => dispatch(getProductList())
    }
}

//TODO: 你这个Header组件绑定App干啥！？
export default connect(
    null, mapDispatchToProps)(App);