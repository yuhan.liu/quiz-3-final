const getNoteList = () => (dispatch) => {
    fetch("http://localhost:8080/api/products").then((response) => {
        response.json().then((result) => {
            dispatch(
                {
                    type: "GET_PRODUCT_LIST",
                    notes: result
                }
            )
        })
    })
};

export default getNoteList;