import React, {Component} from 'react';
import {connect} from 'react-redux';

import {Link} from "react-router-dom";

function mapStateToProps(state) {
    return {
        notes: state.notes.notes
    };
}

class Products extends Component {
    constructor(props, context) {
        super(props, context);
    }

    render() {
        return (
            <div>
                <body>
                <ul>
                    {this.props.notes.map(item => {
                        let uri = '/products/' + item.id;
                        return (<li><Link to={uri}>{item.name}</Link></li>)
                    })}
                </ul>
                </body>
            </div>
        );
    }
}

export default connect(
    mapStateToProps)(Products);