const notes = (state = { notes: [] }, action) => {
    if (action.type === "GET_PRODUCT_LIST") {
        return {
            ...state,
            notes: action.notes
        }
    } else {
        return state;
    }
};
export default notes;