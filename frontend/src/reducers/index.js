import {combineReducers} from "redux";
import notes from "../products/reducers/notes";


const reducers = combineReducers({
    notes,
});
export default reducers;